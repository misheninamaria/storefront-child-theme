<?php
    /* Section where current cart can be shown. */
    /* For now this part was taken from the mini-cart.php template just as base layout */
?>
    <section class="cart products">
        <?php if (!WC()->cart->is_empty()): ?>

            <h2><?php esc_html_e( 'Your current cart:', 'woocommerce' ); ?></h2>

            <ul class="woocommerce-mini-cart cart_list product_list_widget <?php echo esc_attr( $args['list_class'] ); ?>">
                <?php
                do_action( 'woocommerce_before_mini_cart_contents' );

                foreach ( WC()->cart->get_cart() as $cart_item_key => $cart_item ) {
                    $_product     = apply_filters( 'woocommerce_cart_item_product', $cart_item['data'], $cart_item, $cart_item_key );
                    $product_id   = apply_filters( 'woocommerce_cart_item_product_id', $cart_item['product_id'], $cart_item, $cart_item_key );

                    if ( $_product && $_product->exists() && $cart_item['quantity'] > 0 && apply_filters( 'woocommerce_widget_cart_item_visible', true, $cart_item, $cart_item_key ) ) {
                        $product_name      = apply_filters( 'woocommerce_cart_item_name', $_product->get_name(), $cart_item, $cart_item_key );
                        $thumbnail         = apply_filters( 'woocommerce_cart_item_thumbnail', $_product->get_image(), $cart_item, $cart_item_key );
                        $product_price     = apply_filters( 'woocommerce_cart_item_price', WC()->cart->get_product_price( $_product ), $cart_item, $cart_item_key );
                        $product_permalink = apply_filters( 'woocommerce_cart_item_permalink', $_product->is_visible() ? $_product->get_permalink( $cart_item ) : '', $cart_item, $cart_item_key );
                        ?>
                        <li class="woocommerce-mini-cart-item <?php echo esc_attr( apply_filters( 'woocommerce_mini_cart_item_class', 'mini_cart_item', $cart_item, $cart_item_key ) ); ?>">
                            <?php if ( empty( $product_permalink ) ) : ?>
                                <?php echo $thumbnail . $product_name; ?>
                            <?php else : ?>
                                <a href="<?php echo esc_url( $product_permalink ); ?>">
                                    <?php echo $thumbnail . $product_name; ?>
                                </a>
                            <?php endif; ?>
                            <?php echo wc_get_formatted_cart_item_data( $cart_item ); ?>
                            <?php echo apply_filters( 'woocommerce_widget_cart_item_quantity', '<span class="quantity">' . sprintf( '%s &times; %s', $cart_item['quantity'], $product_price ) . '</span>', $cart_item, $cart_item_key ); ?>
                        </li>
                        <?php
                    }
                }

                do_action( 'woocommerce_mini_cart_contents' );
                ?>
            </ul>

            <p class="woocommerce-mini-cart__total total"><strong><?php _e( 'Subtotal', 'woocommerce' ); ?>:</strong> <?php echo WC()->cart->get_cart_subtotal(); ?></p>

        <?php else : ?>

            <p class="woocommerce-mini-cart__empty-message"><?php _e( 'No products in the cart.', 'woocommerce' ); ?></p>

        <?php endif; ?>
    </section>

<?php

    /* Required products section here */
    $products = get_required_products();

    if ($products) :
        /* via such filter we can remove native "add_to_cart" js from the add_to_cart button, simply modify button classes */
        /* In case we need some JS, we can attach it on click on the .custom_add_to_cart_button */
        add_filter( 'woocommerce_loop_add_to_cart_link', 'custom_step_add_to_cart_link', 15, 3);
        function custom_step_add_to_cart_link($url, $product, $args) {
            $url = sprintf( '<a href="%s" data-quantity="%s" class="%s" %s>%s</a>',
                esc_url( $product->add_to_cart_url() ),
                esc_attr( isset( $args['quantity'] ) ? $args['quantity'] : 1 ),
                esc_attr( 'button custom_add_to_cart_button' ),
                isset( $args['attributes'] ) ? wc_implode_html_attributes( $args['attributes'] ) : '',
                esc_html( $product->add_to_cart_text() )
            );
            return $url;
        }
        ?>

    <section class="products-b products">

        <h2><?php esc_html_e( 'In order to proceed with the checkout please select required products:', 'woocommerce' ); ?></h2>

        <?php woocommerce_product_loop_start(); ?>

        <?php foreach ($products as $product) : ?>

            <?php
            $post_object = get_post( $product->ID );

            setup_postdata( $GLOBALS['post'] =& $product );

            wc_get_template_part('content', 'product'); ?>

        <?php endforeach; ?>

        <?php woocommerce_product_loop_end(); ?>

    </section>

<?php endif;

wp_reset_postdata();

remove_filter( 'woocommerce_loop_add_to_cart_link', 'custom_step_add_to_cart_link', 15);