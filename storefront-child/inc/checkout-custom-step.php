<?php
define('ADDITIONAL_CHECKOUT_STEP_SLUG', 'custom-step');
define('REQUIRED_PRODUCTS_CATEGORY_SLUG', 'required-products');
define('SPECIAL_PRODUCTS_CATEGORY_SLUG', 'special-products');

/* Add special categories if they don't exist */
add_action('after_switch_theme', 'add_required_products_category');
function add_required_products_category() {
    if (!get_term_by('slug', REQUIRED_PRODUCTS_CATEGORY_SLUG, 'product_cat'))
        wp_insert_term(__('Required Products', 'storefront-child'), 'product_cat', array('slug' => REQUIRED_PRODUCTS_CATEGORY_SLUG));

    if (!get_term_by('slug', SPECIAL_PRODUCTS_CATEGORY_SLUG, 'product_cat'))
        wp_insert_term(__('Special Products', 'storefront-child'), 'product_cat', array('slug' => SPECIAL_PRODUCTS_CATEGORY_SLUG));
}


/* Add custom endpoint for checkout custom step */
add_action('init', 'add_custom_checkout_page');
function add_custom_checkout_page() {
    $slug = get_checkout_page_slug();
    if (!$slug) return;

    add_rewrite_rule("$slug/" . ADDITIONAL_CHECKOUT_STEP_SLUG, "index.php?pagename=$slug&" . ADDITIONAL_CHECKOUT_STEP_SLUG, 'top');

    add_filter('query_vars', function ($vars) {
        $vars[] = ADDITIONAL_CHECKOUT_STEP_SLUG;
        return $vars;
    });
}


function get_checkout_page_slug() {
    $checkout_page_id = wc_get_page_id('checkout');
    $checkout_page = get_post($checkout_page_id);
    if (!is_a($checkout_page, 'WP_Post')) return;

    return $checkout_page->post_name;
}


function get_custom_step_checkout_url() {
    $checkout_url = wc_get_page_permalink('checkout');

    return wc_get_endpoint_url(ADDITIONAL_CHECKOUT_STEP_SLUG, '', $checkout_url);
}


/* If we on checkout page and have ADDITIONAL_CHECKOUT_STEP_SLUG in the params, we will show different screen for selecting required products */
add_filter('wc_get_template', 'add_custom_endpoint_layout', 10, 5);
function add_custom_endpoint_layout($located, $template_name, $args, $template_path, $default_path) {
    if ($template_name == 'checkout/form-checkout.php') {
        global $wp_query;
        if (isset($wp_query->query[ADDITIONAL_CHECKOUT_STEP_SLUG])) {
            $located = get_stylesheet_directory() . '/templates/checkout/custom-step.php';
        }
    }

    return $located;
}


/* We will create correct url for buttons all over the site */
add_filter('woocommerce_get_checkout_url', function ($checkout_url) {
    if (is_additional_checkout_step_needed())
        $checkout_url = get_custom_step_checkout_url();

    return $checkout_url;
});


/* We need this redirect in case somebody open checkout page directly */
add_action('template_redirect', 'redirect_to_custom_step_if_no_required_products_in_the_cart');
function redirect_to_custom_step_if_no_required_products_in_the_cart() {
    global $wp_query;
    if (is_checkout() && is_additional_checkout_step_needed() && !isset($wp_query->query[ADDITIONAL_CHECKOUT_STEP_SLUG])) {
        wp_redirect(get_custom_step_checkout_url());
        die;
    }
}


/*We need this redirect in case somebody open custom checkout page directly */
add_action('template_redirect', 'redirect_to_checkout_if_required_products_are_in_the_cart');
function redirect_to_checkout_if_required_products_are_in_the_cart() {
    global $wp_query;
    if (is_checkout() && !is_additional_checkout_step_needed() && isset($wp_query->query[ADDITIONAL_CHECKOUT_STEP_SLUG])) {
        wp_redirect(wc_get_page_permalink('checkout'));
        die;
    }
}

/* We need this redirect in case we have only Required Product in the cart
   (optional, can be removed if user can proceed to the checkout with Required Product only)
*/
add_action('template_redirect', 'redirect_to_shop_if_only_required_products_are_in_the_cart');
function redirect_to_shop_if_only_required_products_are_in_the_cart() {
    if (is_checkout() && !is_special_product_in_the_cart() && is_required_product_in_the_cart()) {
        wc_add_notice(__('You have to select more products from Special Products or remove Required Product from the cart to proceed with the checkout', 'storefront-child'), 'notice');
        wp_redirect(wc_get_page_permalink('shop'));
        die;
    }
}


/* Any logic for additional checkout step should be placed here. */
function is_additional_checkout_step_needed() {
    return (if_special_products_exist() && is_special_product_in_the_cart() && if_required_products_exist() && !is_required_product_in_the_cart());
}


/* If we have any products in the Required Products category */
function if_required_products_exist() {
    $terms = get_term_by('slug', REQUIRED_PRODUCTS_CATEGORY_SLUG, 'product_cat');
    if (!$terms || !$terms->count)
        return false;

    return true;
}


/* If we have any products in the Special Products category */
function if_special_products_exist() {
    $terms = get_term_by('slug', SPECIAL_PRODUCTS_CATEGORY_SLUG, 'product_cat');
    if (!$terms || !$terms->count)
        return false;

    return true;
}

/* Check if we already have at least 1 product from Required Products in the cart */
function is_required_product_in_the_cart() {
    if (WC()->cart->is_empty()) return false;

    foreach (WC()->cart->get_cart() as $cart_item_key => $cart_item) {
        if (has_term(REQUIRED_PRODUCTS_CATEGORY_SLUG, 'product_cat', $cart_item['product_id'])) {
            return true;
        }
    }

    return false;
}

/* Check if we already have at least 1 product from Special Products in the cart */
function is_special_product_in_the_cart() {
    if (WC()->cart->is_empty()) return false;

    foreach (WC()->cart->get_cart() as $cart_item_key => $cart_item) {
        if (has_term(SPECIAL_PRODUCTS_CATEGORY_SLUG, 'product_cat', $cart_item['product_id'])) {
            return true;
        }
    }

    return false;
}


/* Logic for required products can be implemented here.
For now we take all products from Required Products Category */
function get_required_products() {
    $args = array(
        'post_type' => 'product',
        'posts_per_page' => -1,
        'tax_query' => array(
            array(
                'taxonomy' => 'product_cat',
                'terms' => REQUIRED_PRODUCTS_CATEGORY_SLUG,
                'field' => 'slug',
            )
        ),
    );
    $query = new WP_Query();

    return $query->query($args);
}

/* Don't let user add more then 1 required product to the cart. - optional, can be removed */
add_filter('woocommerce_add_to_cart_validation', 'check_if_we_already_have_required_product_in_the_cart', 99, 2);
function check_if_we_already_have_required_product_in_the_cart($passed, $added_product_id) {
    if (!has_term(REQUIRED_PRODUCTS_CATEGORY_SLUG, 'product_cat', $added_product_id)) return $passed;

    if (is_required_product_in_the_cart()) {
        wc_add_notice(__('Product from Required Products category is already in the cart!', 'storefront-child'), 'notice');
        return false;
    }

    return $passed;
}


/* --------------- Optional filters -------------------- */
/* Exclude Required Products from [products] shortcode*/
add_filter('shortcode_atts_products', 'exclude_required_products_from_the_loop_in_the_products_shortcode');
function exclude_required_products_from_the_loop_in_the_products_shortcode($args) {
    $args['category'] = REQUIRED_PRODUCTS_CATEGORY_SLUG;
    $args['cat_operator'] = 'NOT IN';
    return $args;
}

;


/* Exclude Required Products from woocommerce regular loop */
add_action('woocommerce_product_query', 'exclude_required_products_from_the_loop');
function exclude_required_products_from_the_loop($query) {
    $tax_query = (array) $query->get('tax_query');

    $tax_query[] = array(
        'taxonomy' => 'product_cat',
        'field' => 'slug',
        'terms' => array(REQUIRED_PRODUCTS_CATEGORY_SLUG),
        'operator' => 'NOT IN'
    );
    $query->set('tax_query', $tax_query);
}


/* Hide Required Products category from [product_categories] shortcode */
add_filter('woocommerce_product_categories', 'exclude_required_products_category_from_products_categories_shortcode');
function exclude_required_products_category_from_products_categories_shortcode($product_categories) {
    if (!$product_categories) return;
    $product_categories = wp_list_filter($product_categories, array(
        'slug' => REQUIRED_PRODUCTS_CATEGORY_SLUG,
    ), 'NOT');

    return $product_categories;
}
